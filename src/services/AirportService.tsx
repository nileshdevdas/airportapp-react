/**
 * we axios in our application so all the get/put/post are based on axios ....
 *
 */
import axios from 'axios';

export const AIRPORTS_URL = 'airports.json';
export const SEARCH_URL = 'search.json';

export default function AirportService(): any {
    return {
        getAirports: () => {
            return axios.get(AIRPORTS_URL);
        },
        searchAirports: (request: any) => {
            return axios.get(SEARCH_URL);
        }
    }
}
