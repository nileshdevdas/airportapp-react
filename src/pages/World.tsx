import {useEffect, useState} from "react";
import ListAirports from "../common/ListAirports";
import Login from "./Login";
import axios from "axios";
import './World.css';
import {SERVICE_BASE_URL} from "../config/IContants";

/**
 * As this is a function component i will need to use useState to get access to state
 * @constructor
 */
export default function World() {
    /**
     the api service instance so i can access the api services
     */
    // const airportService = AirportService();

    /**
     exposing a state variable name count
     exposing a searchValue
     exposing searchResult
     */
    const [count, setCount] = useState(0);
    const [searchValue, setSearchValue] = useState('');
    const [searchResult, setSearchResult] = useState([]);

    /**
     * mounted successfully
     */
    useEffect(() => {
    })

    const handleSearchValue = (e: any) => {
        setSearchValue(e.target.value);
    }
    const handleSearch = (e: any) => {
        const token = sessionStorage.getItem('token');
        axios.get(SERVICE_BASE_URL +"airports", {headers: {Authorization: 'Bearer ' + token}}).then((response) => {
            setCount(response.data.length);
        }).catch(err => console.log(err));
    }
    return (
        <div>
            <h1>
                <img src="/images/logo-196px.png" style={{height: '1.44em'}}></img>
                &nbsp;Our Airports
            </h1>
            <div className='font-weight-bold'>For people who who fly: {count} airports (and counting ...)</div>
            <br></br>
            <div className='row d-flex justify-content-center'>
                <div className='col-md-4'>
                    <form>
                        <label htmlFor='q' className='text-primary'>Look up an airport</label>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" onChange={handleSearchValue}
                                   placeholder="Airport code, city, address, lat/lon, etc."
                                   aria-label="Recipient's username" aria-describedby="basic-addon2">
                            </input>
                            <div className="input-group-append">
                                <span className="input-group-text" id="basic-search"
                                      onClick={handleSearch}>Search</span>
                            </div>
                        </div>
                        <p className="text-center search-links">
                            <i className="fa fa-map-marker" aria-hidden="true"></i>
                            <a id="geolocation" href="javascript:getGeolocation()" className="btn btn-link">
                                Near me
                            </a>
                            <i className="fa fa-question-circle" aria-hidden="true"></i>
                            <a href="/random" className="btn btn-link">
                                Random airport
                            </a>
                        </p>
                    </form>
                </div>
            </div>
            <div className='row d-flex justify-content-center'>
                <ListAirports searchData={[]}/>
            </div>
            <Login/>
        </div>
    )
}
