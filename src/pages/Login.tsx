import axios from "axios";
import {SERVICE_BASE_URL} from "../config/IContants";

export default function Login() {
    const login = (e: any) => {
        console.log("Login Clicked with ", e)
        axios.post(SERVICE_BASE_URL + "login", {username: "nilesh", password: "nilesh"}).then((response) => {
            sessionStorage.setItem("token", response.data.token);
        }).catch((e) => {
            console.log(e.message)
        });
    }
    return (<div>
        <button onClick={login}>Login</button>
    </div>)
}
