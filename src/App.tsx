import React from 'react';
import './App.css';
import NavBar from "./common/NavBar";
import {BrowserRouter, Route} from "react-router-dom";
import World from "./pages/World";
import Airports from "./pages/Airports";
import Comments from "./pages/Comments";
import Downloads from "./pages/Downloads";
import LeaderBoard from "./pages/LeaderBoard";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Footer from "./pages/Footer";
import Oops from "./pages/Oops";

const navitems: any = [
    {label: 'World', url: 'world', component: World, icon: 'fa fa-globe'},
    {label: 'Airports', url: 'airports', component: Airports, icon: 'fa fa-plane'},
    {label: 'Comments', url: 'comments', component: Comments, icon: 'fa fa-pencil'},
    {label: 'Downloads', url: 'downloads', component: Downloads, icon: 'fa fa-download'},
    {
        label: 'Site Info', url: 'siteinfo', icon: 'fa fa-info-circle',
        links: [
            {label: 'Contributor leaderboard', url: 'leaderboard', component: LeaderBoard},
            {label: 'About', url: 'about', component: About},
            {label: 'Contact', url: 'contact', component: Contact}
        ]
    }];

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <NavBar items={navitems}/>
                <div className='mt-4'>
                    <Route path='/world' component={World}></Route>
                    <Route path='/airports' component={Airports}></Route>
                    <Route path='/comments' component={Comments}></Route>
                    <Route path='/downloads' component={Downloads}></Route>
                    <Route path='/leaderboard' component={LeaderBoard}></Route>
                    <Route path='/about' component={About}></Route>
                    <Route path='/contact' component={Contact}></Route>
                    <Route  component={Oops}></Route>
                </div>
            </BrowserRouter>
            <Footer/>
        </div>
    );
}

export default App;
