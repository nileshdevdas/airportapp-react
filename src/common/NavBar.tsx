import {Link} from "react-router-dom";

/**
 * how to use props in the Functional Component...
 * @param props
 * @constructor
 */
export default function NavBar(props: any) {
    return (<nav id='anavbar' className="navbar navbar-expand-sm bg-dark navbar-dark">
            <img src="/images/logo-196px.png" alt="OurAirports home" style={{height: '2em'}}></img>
            <a className="navbar-brand" href="/">Our Airports</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div id="navbarNavDropdown" className="navbar-collapse collapse">

                <ul className="navbar-nav">
                    {props.items.map((navitem: any) => {
                        if (navitem.url === 'siteinfo') {
                            return (<li key={Math.random()} className="nav-item dropdown">
                                <a key={Math.random()} className="nav-link dropdown-toggle" href="#" id="navbardrop"
                                   data-toggle="dropdown">
                                    <i key={Math.random()} className={navitem.icon} aria-hidden="true"></i><span
                                    className='ml-2'>{navitem.label}</span>
                                </a>
                                <div key={Math.random()} className="dropdown-menu">
                                    {navitem.links.map((subitem: any) => {
                                        return (
                                            <Link key={Math.random()} to={subitem.url}>
                                                <a key={Math.random()} className="dropdown-item">{subitem.label}</a>
                                            </Link>);
                                    })}
                                </div>
                            </li>)
                        } else {
                            return (<li key={Math.random()} className="nav-item">
                                <Link key={navitem.label} to={navitem.url}>
                                    <a className="nav-link"> <i className={navitem.icon} aria-hidden="true"></i>
                                        <span className='ml-2'>{navitem.label}</span></a>
                                </Link>
                            </li>);
                        }
                    })}
                </ul>
            </div>
            <ul className="navbar-nav">
                <li className="nav-item">
                    <a className="nav-link" href="{{ url('/login') }}"><i className='fa fa-user mr-2' aria-hidden="true"></i>Login</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="{{ url('/register') }}"><i className='fa  fa-wpforms mr-2' aria-hidden="true"></i> Register</a>
                </li>
            </ul>
        </nav>
    )
}

