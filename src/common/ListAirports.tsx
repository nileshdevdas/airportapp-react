import React, {useState} from "react";
import DataTable from 'react-data-table-component';
import axios from "axios";
import {SERVICE_BASE_URL} from "../config/IContants";

export default function ListAirports({searchData: []}) {
    const columns = [
        {
            name: 'ID',
            selector: (row: any) => row.id,
            sortable: true,

        },
        {
            name: 'Name',
            selector: (row: any) => row.name,
            sortable: true,

        },
        {
            name: 'Airport Type',
            selector: (row: any) => row.type,
            sortable: true,
        },
        {
            name: 'latitude_deg',
            selector: (row: any) => row.latitude_deg,
            sortable: true,
        },
        {
            name: 'longitude_deg',
            selector: (row: any) => row.longitude_deg,
            sortable: true,
        },
        {
            name: 'Country',
            selector: (row: any) => row.iso_country,
            sortable: true,
        },
        {
            name: 'Municipality',
            selector: (row: any) => row.municipality,
            sortable: true,
        },
    ];
    const [data, setData] = useState([]);
    let loadData = () => {
        axios.get(SERVICE_BASE_URL + 'airports', {
            headers: {
                Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzMzMjM0Njk2MzYsImV4cCI6MTYzMzMyMzQ2OTgxNiwiaXNzdWVyIjoibmlsZXNoIiwibmFtZSI6IkpvaG4gRG9lIn0.bD5ROzeGmToHYKhN5ZdcgkC7LcOcHpfV3VeaeMTxhv4'
            }
        }).then((result) => {
            setData(result.data);
        })
    }
    const handleRowClicked = (e: any) => {
        alert("you clicked row with id " + e.id);
    }
    return (
        <div className='col-md-8'>
            <div className='btn btn-success' onClick={loadData}>Load</div>
            <DataTable
                pagination
                columns={columns}
                data={data}
                dense
                selectableRows
                onRowClicked={handleRowClicked}
            />
        </div>
    );
}
