
//BDD this talks more of the end user requirement --->
describe("Suite for Home Page ", ()=>{
    beforeEach(()=>{
        cy.visit("http://localhost/");
    })
    it("Should have Page Title OurAirports", ()=>{
        cy.get('head title').should('contain.text', 'OurAirports');
    });
    it("Should have 7 menu links", ()=>{
        cy.get("nav ul li").should("have.length", 7)
    });
    it("Should navigate to world page when world is clicked", ()=>{

    });
});
