export default {}

describe("World Page Scenarios ", () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000/world")
    })

    it("Must have a title React App", () => {
        cy.get("head title").should("have.text", "React App");
    })

    it("Should have navbar with 5 menus ", () => {
        cy.get("nav ul li").should("have.length", 5);
    })
});

