

export default {}

describe("Test My Application ", ()=>{
    beforeEach(()=>{
        cy.visit("http://localhost:3000")
    })

    it("Must have title React App", ()=>{
        cy.get("head title").should("contain.text", "React App");
    })

    it("Should have a Navigation bar with 5 Naviatins ", ()=>{
        cy.get("#anavbar > ul > li").should("have.length", 5);
    })
});

