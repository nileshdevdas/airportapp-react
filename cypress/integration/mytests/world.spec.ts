export {}

describe("Home Page", () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/world');
    })

    it("Should have header with Our Airports", () => {
        cy.get("#root > div > div > div > h1").should("contain.text", "Our Airports");
    });

    it("Must have Message", () => {
        cy.get('#root > div > div.mt-4 > div > div.font-weight-bold').should('contain.text', 'For people who who fly')
    });

    it("Navigation Bar must have Logo Having Alt", () => {
        cy.get('nav').get('img').should('have.attr', "alt").and("eq", "OurAirports home");
    });

    it("Must have a World Navigation Link", () => {
        cy.get("nav ul li", {log: true}).should("have.length", "5");
    });


});

