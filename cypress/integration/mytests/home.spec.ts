export {}

describe("Home Page", () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000');
    })

    it("Must Have A Navigation Bar", () => {
        cy.get('nav').should('have.id', 'anavbar')
    });

    it("Navigation Bar must have Logo Having Alt ", () => {
        cy.get('nav').get('img').should('have.attr', "alt").and("eq", "OurAirports home");
    })

    it("Must have a World Navigation Link", () => {
        cy.get("nav ul li", {log: true}).should("have.length", "5");
    })

});

